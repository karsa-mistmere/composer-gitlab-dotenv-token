# Composer Gitlab Dotenv Token

A simple composer plugin that loads private Gitlab deploy tokens for composer repositories from environment variables.

Usage:

`composer require karsamistmere/composer-gitlab-dotenv-token`

In your environment variables add:
```
GITLAB_TOKEN={"gitlab.com": "privatetoken"}
```
or
```
GITLAB_TOKEN={"gitlab.com": {"username": "gitlabuser", "token": "privatetoken"}}
```
or
```
GITLAB_TOKEN_HOST=gitlab.com
GITLAB_TOKEN_USERNAME=gitlabuser
GITLAB_TOKEN_TOKEN=privatetoken
```
