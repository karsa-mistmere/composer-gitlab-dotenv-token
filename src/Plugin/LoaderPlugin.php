<?php


namespace ComposerGitlabDotenvToken\Plugin;


use Composer\Composer;
use Composer\EventDispatcher\EventSubscriberInterface;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginEvents;
use Composer\Plugin\PluginInterface;
use Symfony\Component\Dotenv\Dotenv;

class LoaderPlugin implements PluginInterface, EventSubscriberInterface {

  protected Composer $composer;

  protected IOInterface $io;

  public static function getSubscribedEvents() {
    return [
      PluginEvents::INIT => 'addGitlabTokenConfig',
    ];
  }

  public function activate(Composer $composer, IOInterface $io) {
    $this->composer = $composer;
    $this->io       = $io;
  }

  public function deactivate(Composer $composer, IOInterface $io) {
  }

  public function uninstall(Composer $composer, IOInterface $io) {
  }

  public function addGitlabTokenConfig($event) {
    try {
      $dotenv = new Dotenv();
      $dotenv->loadEnv('./.env');
    } catch(\Exception $exception) {
    }
    $config = $this->composer->getConfig();
    $config->merge(['config' => ['gitlab-token' => $this->loadTokenConfig()]]);
    foreach ($this->loadTokenConfig() as $host => $token) {
      if (is_string($token) && !empty($token)) {
        $this->io->setAuthentication($host, $token);
      }
      elseif (!empty($token['username']) && !empty($token['token'])) {
        $this->io->setAuthentication($host, $token['username'], $token['token']);
      }
    }
  }

  private function loadTokenConfig() {
    try {
      $jsonConfig = $_SERVER['GITLAB_TOKEN'] ?? NULL;
      if ($jsonConfig) {
        return json_decode($jsonConfig, TRUE);
      }
    } catch (\Exception $exception) {

    }

    $gitlab_host = $_SERVER['GITLAB_TOKEN_HOST'] ?? 'gitlab.com';
    if (isset($_SERVER['GITLAB_TOKEN_USERNAME'])) {
      return [
        $gitlab_host => [
          'username' => $_SERVER['GITLAB_TOKEN_USERNAME'] ?? NULL,
          'token'    => $_SERVER['GITLAB_TOKEN_TOKEN'] ?? NULL,
        ],
      ];
    }

    return [
      $gitlab_host => $_SERVER['GITLAB_TOKEN_TOKEN'] ?? NULL,
    ];
  }

}
